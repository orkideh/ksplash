import { AnyAction } from '@reduxjs/toolkit';
import { MyImage, MyImageWithLike } from '../types/types';
import reducer, { addToFavorite, removeFromFavorite, selectAllImages } from './favorites-slice';
import { images } from '../assets/mocks/images';
import { useAppSelector } from '../app/hooks';

function extendWithLike(image: MyImage): MyImageWithLike {
    return Object.assign({}, image, { like: true });
}

let preloadedState: MyImageWithLike[];
const emptyAction: AnyAction = { type: '' };


beforeEach(() => {
    preloadedState = images.slice(1, 5).map(elm => extendWithLike(elm));
});

test('should return the initial state', () => {
    const result = reducer(undefined, emptyAction);

    expect(result).toEqual([])
})

test('should handle a todo being added to an empty list', () => {
    const previousState = preloadedState
    const image = extendWithLike(images[0]);

    const result = reducer(previousState, addToFavorite(image));
    const expected = [
        ...preloadedState,
        image,
    ];
    expect(result).toEqual(expected);
})

test('should handle a todo being added to an empty list', () => {
    const previousState = preloadedState;
    const image: MyImageWithLike = preloadedState[0];

    const result = reducer(previousState, removeFromFavorite(image));
    const expected = preloadedState.filter(elm => elm.id !== image.id);

    expect(result.length).toEqual(expected.length);
})
