import {createSlice, PayloadAction} from '@reduxjs/toolkit'

import { RootState } from '../app/store';
import { MyImage, MyImageWithLike } from '../types/types'
import { FavoritesStorage } from '../utils/utils';

export type FavoritesState = MyImageWithLike[];

const MyStorage = new FavoritesStorage();
const initialState: FavoritesState = MyStorage.get();

const favoritesSlice = createSlice({
    name: 'favorites',
    initialState,
    reducers: {
        addToFavorite(state, action: PayloadAction<MyImage>) {
            // it is ok to do it, reduxtool uses emmer inside to convert it immutable 
            // https://immerjs.github.io/immer/
            const imageIndex = state.findIndex(image => image.id === action.payload.id);
            const payloadToAdd = Object.assign({}, action.payload, {like: true});
            if(imageIndex === -1) {
                state.push(payloadToAdd);
            }
            MyStorage.set(state);
        },
        removeFromFavorite(state, action: PayloadAction<MyImageWithLike>) {   
            // it is ok to do it, reduxtool uses emmer inside to convert it immutable 
            // https://immerjs.github.io/immer/
            const imageIndex = state.findIndex(image => image.id === action.payload.id);         
            state.splice(imageIndex, 1)
            MyStorage.set(state);
        }
    }
})

export const { addToFavorite, removeFromFavorite } = favoritesSlice.actions;

export const selectAllImages = (state: RootState) => state.favorites

export const selectImageById = (id: string) => (state: RootState) => {
    return state.favorites.find(image => image.id === id); 
}

export default favoritesSlice.reducer;
