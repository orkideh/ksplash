import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { MyImage } from '../../types/types';
import config from '../../config/config.json';

const apiBaseUrl = config.dev.enabled ? '/' : 'https://api.unsplash.com';

export const imageApiSlice = createApi({
    reducerPath: 'images',
    keepUnusedDataFor: 600,
    baseQuery: fetchBaseQuery({
        baseUrl: apiBaseUrl,
        prepareHeaders(headers) {
            headers.set('Authorization', `Client-ID ${config.auth.token}`);
            return headers;
        },
    }),
    endpoints(builder) {
        return {
            fetchImages: builder.query<MyImage[], number | void>({
                query(count = 50) {
                    return `/photos/random?count=${count}`
                }
            }),
            fetchImage: builder.query<MyImage, string |  void>({
                query(id) {
                    return `/photos/${id}/`;
                },
            })
        };
    }
});


export const { useFetchImagesQuery, useFetchImageQuery } = imageApiSlice;