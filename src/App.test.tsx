import React from 'react';
import { Home } from './components/home/home.component';
import { images } from './assets/mocks/images';
import { rest } from 'msw'
import { setupServer } from 'msw/node'
import { render, fireEvent, screen } from './test-utils'
import App from './app.component';
import { waitFor, waitForElementToBeRemoved } from '@testing-library/react';
import ReactDOM from 'react-dom';

export const handlers = [
  rest.get('/photos/random', (req, res, ctx) => {
    return res(ctx.json(images))
  }),
  rest.get('/photos/:id', (req, res, ctx) => {
    const { id } = req.params;
    const image = images.find(elm => elm.id === id);
    return res(ctx.json(image), ctx.delay(150))
  })
];

const server = setupServer(...handlers)

beforeAll(() => {
  ReactDOM.createPortal = jest.fn((element): any => {
    return element
  }) as any;
  server.listen()
});

afterEach(() => {
  (ReactDOM.createPortal as any).mockClear();
  server.resetHandlers()
});

afterAll(() => server.close());

test('should render App with router home with 30 images from mocked API', async () => {
  const { baseElement, container, debug } = render(<App />);

  await waitFor(() => screen.getByTestId('masonry-grid'));
  const imagesRendered = container.querySelectorAll('.masonry-card');
  const menuRendered = screen.getByTestId('fixed-menu');

  expect(imagesRendered.length).toBe(30);
  expect(menuRendered).toBeInTheDocument();
});

test('should render App with router four o four with /noroute', async () => {
  const { baseElement, container, debug } = render(<App />, '/noroute');

  const fourOFourElement = screen.getByText(/Unfortunately your requested page has not found/i);
  expect(container).toMatchSnapshot();
});

test('should render App with router modal page with correct image id', async () => {
  const { baseElement, container, debug } = render(<App />, `/photos/${images[0].id}`);

  const fixedMenu = await screen.getByTestId('fixed-menu');
  expect(fixedMenu.parentElement?.parentElement?.className).toBe('no-overflow');
  expect(container).toMatchSnapshot();
});
