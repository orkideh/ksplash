import { FavoritesState } from "../feature/favorites-slice";
import { MyImage, MyImageWithLike } from "../types/types";

export class FavoritesStorage {
    private key: string
    constructor() {
        this.key = 'favorites';
    }
    get(): FavoritesState {
        const item = localStorage.getItem(this.key);
        return item ? JSON.parse(item) : [];
    }
    set(item: FavoritesState) {
        localStorage.setItem(this.key, JSON.stringify(item))
    }
}

export function randomNumberRange(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min) + min);
}

export function debounce_leading(this: any, func: (...args: any) => any, timeout = 300) {
    let timer: any;
    return (...args: any) => {
        if (!timer) {
            func.apply(this, args);
        }
        clearTimeout(timer);
        timer = setTimeout(() => {
            timer = undefined;
        }, timeout);
    };
}

export function mapImagesWithLikes(images: MyImage[], favorites: MyImage[]): MyImageWithLike[] {
    return images.map(image => {
        return {
            ...image,
            like: favorites.findIndex(favorite => favorite.id === image.id) > -1
        };
    });
}

export function getImageUrlWidth(imageUrl: string) {
    const url = new URL(imageUrl);
    return url.searchParams.get('w');
}

export function generateSrcSet(urls: MyImage['urls']) {
    return `${urls.full} 1000w,    
    ${urls.regular} ${getImageUrlWidth(urls.regular)}w,    
    ${urls.small} ${getImageUrlWidth(urls.small)}w,    
    ${urls.thumb} ${getImageUrlWidth(urls.thumb)}w`;
}

