import React from "react";
import { images } from "../assets/mocks/images";
import { MyImage, MyImageWithLike } from "../types/types";
import { getImageUrlWidth, randomNumberRange, mapImagesWithLikes, generateSrcSet, FavoritesStorage } from "./utils";

let image: MyImage;
let imagesSubset: MyImage[];
let imageswithLikesSubset: MyImageWithLike[];

beforeEach(() => {
    image = images[0];
    imagesSubset = images.slice(0, 5);
    imageswithLikesSubset = images.slice(2, 7).map(item => ({ ...item, like: true }));

});

test('randomNumberRange should be in range', () => {
    const result = randomNumberRange(2, 20);
    expect(result).toBeGreaterThanOrEqual(2);
    expect(result).toBeLessThanOrEqual(20);
});

test('getImageUrlWidth should add with sample url data', () => {
    const sampleUrl = 'http://google.com/?data=something&w=200'
    const result = getImageUrlWidth(sampleUrl);
    expect(result).toBe('200');
});

test('mapImagesWithLikes should add likes to every entry', () => {
    const resultSubSet = mapImagesWithLikes(imagesSubset, imageswithLikesSubset);
    const resultSubsetWithLikesTrue = resultSubSet.filter(elm => elm.like);
    const resultSubsetWithLikesFalse = resultSubSet.filter(elm => !elm.like);
    expect(resultSubsetWithLikesTrue.length).toBe(3);
    expect(resultSubsetWithLikesFalse.length).toBe(2);

});

test('generateSrcSet should render curretly based on passed image', () => {
    const urls = {
        raw: 'https://images.unsplash.com/photo-1557776198-8b17010cbbb7?ixid=MnwzMTgwMzF8MHwxfHJhbmRvbXx8fHx8fHx8fDE2NDk5NjgxMzU&ixlib=rb-1.2.1',
        full: 'https://images.unsplash.com/photo-1557776198-8b17010cbbb7?crop=entropy&cs=srgb&fm=jpg&ixid=MnwzMTgwMzF8MHwxfHJhbmRvbXx8fHx8fHx8fDE2NDk5NjgxMzU&ixlib=rb-1.2.1&q=85',
        regular: 'https://images.unsplash.com/photo-1557776198-8b17010cbbb7?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwzMTgwMzF8MHwxfHJhbmRvbXx8fHx8fHx8fDE2NDk5NjgxMzU&ixlib=rb-1.2.1&q=80&w=1080',
        small: 'https://images.unsplash.com/photo-1557776198-8b17010cbbb7?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwzMTgwMzF8MHwxfHJhbmRvbXx8fHx8fHx8fDE2NDk5NjgxMzU&ixlib=rb-1.2.1&q=80&w=400',
        thumb: 'https://images.unsplash.com/photo-1557776198-8b17010cbbb7?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwzMTgwMzF8MHwxfHJhbmRvbXx8fHx8fHx8fDE2NDk5NjgxMzU&ixlib=rb-1.2.1&q=80&w=200',
        small_s3: 'https://s3.us-west-2.amazonaws.com/images.unsplash.com/small/photo-1557776198-8b17010cbbb7'
    };
    const expected = `https://images.unsplash.com/photo-1557776198-8b17010cbbb7?crop=entropy&cs=srgb&fm=jpg&ixid=MnwzMTgwMzF8MHwxfHJhbmRvbXx8fHx8fHx8fDE2NDk5NjgxMzU&ixlib=rb-1.2.1&q=85 1000w,    
    https://images.unsplash.com/photo-1557776198-8b17010cbbb7?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwzMTgwMzF8MHwxfHJhbmRvbXx8fHx8fHx8fDE2NDk5NjgxMzU&ixlib=rb-1.2.1&q=80&w=1080 1080w,    
    https://images.unsplash.com/photo-1557776198-8b17010cbbb7?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwzMTgwMzF8MHwxfHJhbmRvbXx8fHx8fHx8fDE2NDk5NjgxMzU&ixlib=rb-1.2.1&q=80&w=400 400w,    
    https://images.unsplash.com/photo-1557776198-8b17010cbbb7?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwzMTgwMzF8MHwxfHJhbmRvbXx8fHx8fHx8fDE2NDk5NjgxMzU&ixlib=rb-1.2.1&q=80&w=200 200w`;

    expect(generateSrcSet(urls)).toEqual(expected)
});

test('FavoritesStorage should behave as expected', () => {
    const set = jest.spyOn(localStorage.__proto__, 'setItem');
    const get = jest.spyOn(localStorage.__proto__, 'getItem');

    const storage = new FavoritesStorage();
    storage.set(imageswithLikesSubset);

    const setExpected = JSON.stringify(imageswithLikesSubset);
    expect(set).toBeCalledWith('favorites', setExpected);

    const getResult = storage.get();
    const getExpected = JSON.stringify(imageswithLikesSubset);

    expect(getResult).toEqual(imageswithLikesSubset);
});
