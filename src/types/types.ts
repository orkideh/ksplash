// Only in order to use the types
import { Full } from 'unsplash-js/dist/methods/photos/types';

export type MyImage = Full

export type MyImageWithLike = MyImage & { like: boolean }
