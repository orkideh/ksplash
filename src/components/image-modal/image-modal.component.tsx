import React from "react";
import { useParams } from "react-router-dom";
import { useImage } from "../../app/hooks";
import { CustomError } from "../ui/custom-error/custom-error.component";
import { ImageModalExif } from "../ui/image-modal-exif/image-modal-exif.component";
import { ImageModalHeader } from "../ui/Image-modal-header/image-modal-header.component";
import { Loading } from "../ui/loading/loading.component";
import { Modal } from "../ui/modal/modal.component";
import { ResponsiveModalImage } from "../ui/responsive-modal-image/responsive-modal-image.component";

import styles from './image-modal.module.scss';

export function ImageModal() {
    const { id = '' } = useParams<'id'>();
    const { image, handleToggle, isError } = useImage(id);
    return (
        <Modal>
            {isError ?
                <CustomError /> :
                (image && image.id) ? (
                    <div
                        className={styles.container}
                        data-testid="image-modal"
                    >
                        <div>
                            <div className={styles['image-container']}>
                                <ResponsiveModalImage
                                    image={image}
                                />
                            </div>
                        </div>

                        <div className={styles['bg--grey']}>
                            <div className={styles['container__bio']}>
                                <div>
                                    <ImageModalHeader
                                        image={image}
                                        toggle={handleToggle}
                                    />
                                </div>
                                <div></div>
                                <div>
                                    <ImageModalExif
                                        exif={image.exif}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                ) : <Loading />
            }
        </Modal>

    );
}