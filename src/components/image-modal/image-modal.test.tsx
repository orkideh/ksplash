import { fireEvent, waitFor } from "@testing-library/react";
import React from "react";
import { rest } from 'msw'
import { setupServer } from 'msw/node';

import { render, screen } from '../../test-utils';
import { images } from "../../assets/mocks/images";
import ReactDOM from "react-dom";
import { ImageModal } from "./image-modal.component";
import App from "../../app.component";
import Router, { useParams } from "react-router-dom";
import { MyImage } from "../../types/types";

jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'), 
    useParams: () => ({
        id: 'H_rNGYXbMFA'
    }),
}));

jest.mock('react-dom', () => ({
    ...jest.requireActual('react-dom'),
    createPortal: (element: any) => element
}));

export const handlers = [
    rest.get('/photos/random', (req, res, ctx) => {
        return res(ctx.json(images))
    }),
    rest.get('/photos/:id', (req, res, ctx) => {
        const { id } = req.params;
        const image = images.find(elm => elm.id === id);
        return res(ctx.json(image), ctx.delay(150))
    })
];

const server = setupServer(...handlers);
let image: MyImage;

beforeAll(() => {
    image = images[0];

    jest.spyOn(console, 'error').mockImplementation(() => { });
    server.listen();
});

afterEach(() => {
    server.resetHandlers();
});

afterAll(() => {
    server.close();
    (console.error as any).mockClear();
    jest.resetAllMocks();
});

test('should render Image component', async () => {
    const { container, debug } = render(<App />, `/photos/${image.id}`);

    await waitFor(() => screen.getByTestId('image-modal'));

    const imageContainer = screen.getByTestId('image-modal');
    const responsiveImageComponent = screen.getByTestId('responsive-image');
    const imageModalHeaderComponent = screen.getByTestId('image-modal-header');
    const ImageExifListComponent = screen.getByTestId('image-modal-exif');
    
    expect(imageContainer).toMatchSnapshot();
    expect(responsiveImageComponent).toMatchSnapshot();
    expect(imageModalHeaderComponent).toMatchSnapshot();
    expect(ImageExifListComponent).toMatchSnapshot();
});

