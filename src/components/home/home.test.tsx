import { fireEvent, waitFor } from "@testing-library/react";
import React from "react";
import { rest } from 'msw'
import { setupServer } from 'msw/node';

import { render, screen } from '../../test-utils';
import { Home } from "./home.component";
import { images } from "../../assets/mocks/images";
import ReactDOM from "react-dom";

export const handlers = [
    rest.get('/photos/random', (req, res, ctx) => {
        return res(ctx.json(images))
    }),
    rest.get('/photos/:id', (req, res, ctx) => {
        const { id } = req.params;
        const image = images.find(elm => elm.id === id);
        return res(ctx.json(image), ctx.delay(150))
    })
];

const server = setupServer(...handlers);



describe('when server is down', () => {

    beforeEach(() => {
        jest.spyOn(console, 'error').mockImplementation(() => { });
    });

    test('should handle error', async () => {
        const { container, debug } = render(<Home />);

        await waitFor(() => screen.getByTestId('custom-error'));

        const errorElement = screen.getByText(/We have faced with an error. Please contact our customer support./i);
        expect(errorElement).toBeInTheDocument();
        expect(console.error).toBeCalledTimes(1);

        expect(container).toMatchSnapshot();
    });

    afterEach(() => {
        (console.error as any).mockClear();
    });

});

describe('When server is up', () => {
    beforeAll(() => {
        ReactDOM.createPortal = jest.fn((element): any => {
            return element
        }) as any;

        server.listen();
    });

    afterEach(() => {
        (ReactDOM.createPortal as any).mockClear();
        server.resetHandlers();
    });

    afterAll(() => {
        server.close();
    });

    test('should render Home component', async () => {
        const { container, debug } = render(<Home />, `/photos/${images[0].id}`);

        await waitFor(() => screen.getByTestId('masonry-grid'));

        const grid = screen.getByTestId('masonry-grid');
        expect(grid).toMatchSnapshot();
    });
});

