import React from "react";
import { Outlet } from "react-router-dom";
import { useImageView } from "../../app/hooks";
import { CustomError } from "../ui/custom-error/custom-error.component";
import { FixedMenu } from "../ui/fixed-menu/fixed-menu.component";

import { Layout } from "../ui/layout/layout.component";
import { Loading } from "../ui/loading/loading.component";
import { MasonryGrid } from "../ui/masonry-grid/masonry-grid.component";

export function Home() {
    const { images, isHome, setHomeView, setFavoriteView, homeStatus } = useImageView();

    return (
        <>
            <FixedMenu setFavoriteView={setFavoriteView} setHomeView={setHomeView} />
            <Layout fluid={true}>
                <Outlet />
                <Layout>
                    {
                        isHome && homeStatus.isError
                         ? <CustomError />
                         : isHome && (homeStatus.isFetching || homeStatus.isLoading)
                            ? <Loading />
                            : <MasonryGrid images={images} />
                    }
                </Layout>
            </Layout>
        </>
    );
}
