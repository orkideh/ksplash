import React from "react";

import styles from './custom-error.module.scss';

export function CustomError() {
    return (
        <div
            className={styles['custom-error']}
            data-testid="custom-error"
        >
            <h3>
                We have faced with an error. Please contact our customer support.
            </h3>
        </div>
    );
}