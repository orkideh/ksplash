import React from "react";
import { render, screen } from '../../../test-utils';

import { CustomError } from "./custom-error.component";

test('should render custom error component', () => {
    const { container } = render(<CustomError />);

    const errorElement = screen.getByText(/We have faced with an error. Please contact our customer support./i);
    expect(errorElement).toBeInTheDocument();
    expect(errorElement.parentElement?.className).toBe('custom-error');

    expect(container).toMatchSnapshot();
});