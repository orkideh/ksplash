import React from "react";
import { MyImageWithLike } from "../../../types/types";
import { Like } from "../icons/like/like.component";

import styles from './image-modal-header.module.scss';

interface ImageModalHeaderProps {
    toggle: () => void;
    image: MyImageWithLike
}

export function ImageModalHeader({ toggle, image }: ImageModalHeaderProps) {
    return (
        <header
            data-testid="image-modal-header"
            className={styles.header}
        >
            <div>
                <div
                    onClick={toggle}
                    className={[styles['like-btn'], image.like ? styles['like-btn--liked'] : ''].join(' ')}
                    data-testid="like-btn"
                >
                    <Like />
                    <span className={styles['like-btn__text']}>
                        {image.like ? 'Unline' : 'Like'}
                    </span>
                </div>
            </div>
            <div
                className={styles.title}
                data-testid="image-modal-header-title"
            >
                <h4>
                    {image.description || 'No title'}
                </h4>
            </div>
            <div
                className={styles.bio}
                data-testid="image-modal-header-bio"
            >
                <a href={image.user.links.html} target="_blank" rel="noopener">
                    <img
                        src={image.user.profile_image.small}
                        alt={image.user.name}
                    />
                    <span>
                        {image.user.name}
                    </span>
                </a>
            </div>
        </header>
    );
}