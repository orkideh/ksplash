import React from "react";
import { images } from "../../../assets/mocks/images";
import { render, screen } from '../../../test-utils';
import { MyImage, MyImageWithLike } from "../../../types/types";
import { ImageModalHeader } from "./image-modal-header.component";

let image: MyImageWithLike;
let toggle: () => void;

beforeEach(() => {
    image = Object.assign({}, images[0], { like: false })
    toggle: jest.fn()
});

test('should render image modal header component', () => {
    const { container, debug } = render(<ImageModalHeader image={image} toggle={toggle} />);

    const headerElement = screen.getByTestId('image-modal-header');
    expect(headerElement.className).toBe('header');

    const likeElement = screen.getByTestId('like-btn');
    expect(likeElement.classList.contains('like-btn')).toBe(true);

    const titleElement = screen.getByTestId('image-modal-header-title');
    expect(titleElement.classList.contains('title')).toBe(true);

    const bioElement = screen.getByTestId('image-modal-header-bio');
    expect(bioElement.classList.contains('bio')).toBe(true);
    expect(container).toMatchSnapshot();
});


test('should render liked icon when like prop is TRUE', () => {
    image.like = true
    const { container, debug } = render(<ImageModalHeader image={image} toggle={toggle} />);

    const headerElement = screen.getByTestId('image-modal-header');
    expect(headerElement.className).toBe('header');

    const likeElement = screen.getByTestId('like-btn');
    expect(likeElement.classList.contains('like-btn--liked')).toBe(true);

    const likeBtnElement = screen.getByText(/Unline/i);
    expect(likeBtnElement).toBeInTheDocument();
    expect(likeBtnElement.className).toBe('like-btn__text');
    expect(container).toMatchSnapshot();
});

test('should render liked icon when like prop is FALSE', () => {
    image.like = false
    const { container, debug } = render(<ImageModalHeader image={image} toggle={toggle} />);

    const headerElement = screen.getByTestId('image-modal-header');
    expect(headerElement.className).toBe('header');

    const likeElement = screen.getByTestId('like-btn');
    expect(likeElement.classList.contains('like-btn--liked')).toBe(false);

    const likeBtnElement = screen.getByText(/like/i);
    expect(likeBtnElement).toBeInTheDocument();
    expect(likeBtnElement.className).toBe('like-btn__text');
    expect(container).toMatchSnapshot();
});

test('should render title when description prop has a value', () => {
    image.description = image.description || 'sample description just for test';
    const { container, debug } = render(<ImageModalHeader image={image} toggle={toggle} />);

    const titleElement = screen.getByTestId('image-modal-header-title');
    expect(titleElement).toBeInTheDocument();
    expect(container).toMatchSnapshot();
});

test('should render default title when description prop is empty', () => {
    image.description = '';
    const { container, debug } = render(<ImageModalHeader image={image} toggle={toggle} />);

    const titleElement = screen.getByTestId('image-modal-header-title');
    expect(titleElement).toBeInTheDocument();
    expect(container).toMatchSnapshot();
});

test('should render bio based on passed image', () => {
    const { container, debug } = render(<ImageModalHeader image={image} toggle={toggle} />);

    const userBioElement = screen.getByAltText(`${image.user.name}`);
    expect(userBioElement).toBeInTheDocument();

    expect(userBioElement.getAttribute('src')).toBe(image.user.profile_image.small);
    expect(userBioElement.parentElement?.getAttribute('href')).toBe(image.user.links.html);

    const userBioUserNameElement = screen.getByText(`${image.user.name}`);
    expect(userBioUserNameElement).toBeInTheDocument();
    expect(container).toMatchSnapshot();
});


