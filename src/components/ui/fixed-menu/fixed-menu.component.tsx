import React from "react";
import { HomeIcon } from "../icons/home/images-icon.component";
import { Like } from "../icons/like/like.component";
import { LogoMenu } from "../icons/logo-menu/logo-menu.component";

import styles from './fixed-menu.module.scss';

interface FixedMenuProps {
    setHomeView: () => void;
    setFavoriteView: () => void
};

export function FixedMenu({ setFavoriteView, setHomeView }: FixedMenuProps) {
    return (
        <div
            className={styles.menu}
            data-testid="fixed-menu"
        >
            <span
                className={styles.logo}
                data-testid="fixed-menu-logo-container"
            >
                <LogoMenu />
            </span>
            <div
                className={styles['view-links']}
                data-testid="fixed-menu-view-links"
            >
                <span
                    onClick={setHomeView}
                >
                    <HomeIcon />
                </span>
                <span
                    onClick={setFavoriteView}
                    data-testid="like-container"
                >
                    <Like />
                </span>
            </div>
        </div>
    );
}