import { fireEvent } from "@testing-library/react";
import React from "react";
import { render, screen } from '../../../test-utils';
import { FixedMenu } from "./fixed-menu.component";

test('should render simple component', () => {
    const setHomeView = jest.fn();
    const setFavoriteView = jest.fn();

    const { debug, container } = render(<FixedMenu setFavoriteView={setFavoriteView} setHomeView={setHomeView} />);

    const parent = screen.getByTestId('fixed-menu');
    const logo = screen.getByTestId('fixed-menu-logo-container');
    const linksView = screen.getByTestId('fixed-menu-view-links');

    expect(parent.className).toBe('menu');
    expect(logo.className).toBe('logo');
    expect(linksView.className).toBe('view-links');
    expect(linksView.children.length).toBe(2)
});

test('should call setHomeView callback on home icon click', () => {
    const setHomeView = jest.fn();
    const setFavoriteView = jest.fn();

    const { debug, container } = render(<FixedMenu setFavoriteView={setFavoriteView} setHomeView={setHomeView} />);

    const linksView = screen.getByTestId('fixed-menu-view-links');
    const homeLink = screen.getByTestId('home');

    fireEvent(
        homeLink,
        new MouseEvent('click', {
            bubbles: true,
            cancelable: true,
        })
    );

    expect(setHomeView).toBeCalled();

    expect(container).toMatchSnapshot();
});

test('should call setFavoriteView callback on favorite icon click', () => {
    const setHomeView = jest.fn();
    const setFavoriteView = jest.fn();

    const { debug, container } = render(<FixedMenu setFavoriteView={setFavoriteView} setHomeView={setHomeView} />);

    const linksView = screen.getByTestId('fixed-menu-view-links');
    const likeContainer = screen.getByTestId('like-container');

    fireEvent(
        likeContainer,
        new MouseEvent('click', {
            bubbles: true,
            cancelable: true,
        })
    );

    expect(setFavoriteView).toBeCalled();
    expect(container).toMatchSnapshot();
});
