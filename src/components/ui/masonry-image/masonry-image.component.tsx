import React, { memo } from "react";
import { NavLink } from "react-router-dom";

import { MyImageWithLike } from "../../../types/types";
import { Like } from "../icons/like/like.component";

import styles from './masonry-image.module.scss';

interface MasonryImageProps {
    image: MyImageWithLike,
}

export function MasonryImage({ image }: MasonryImageProps) {
    return (
        <div
            className={styles['masonry-card']}
            style={{
                'backgroundImage': `url('${image.urls.small}')`
            }}
            data-testid="masonry-card"
        >
            <NavLink
                to={`/photos/${image.id}`}
                className={styles.link}
                data-testid="masonry-card-link"
            >
                {image.like ?
                    <span
                     className={styles.like}
                     data-testid="masonry-card-like-container"
                     >
                        <Like />
                    </span>
                    : null}
            </NavLink>
        </div>
    );
}

export const MemorizedMasonryImage = memo(MasonryImage);
