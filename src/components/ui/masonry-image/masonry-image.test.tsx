import React from "react";
import { images } from "../../../assets/mocks/images";
import { render, screen } from '../../../test-utils';
import { MyImageWithLike } from "../../../types/types";
import { randomNumberRange } from "../../../utils/utils";
import { MasonryImage } from "./masonry-image.component";


let image: MyImageWithLike;
let imageLiked: MyImageWithLike;
let imageNotLiked: MyImageWithLike;
beforeEach(() => {
    image = Object.assign({}, images[0], { like: true });
    imageLiked = Object.assign({}, images[1], { like: true });
    imageNotLiked = Object.assign({}, images[2], { like: false });
});

test('should render a simple masonry image component', () => {
    const { debug, container } = render(<MasonryImage image={image} />);

    const masonryElement = screen.getByTestId('masonry-card')

    expect(masonryElement.className).toBe('masonry-card');
    expect(masonryElement.getAttribute('style')).toMatch(image.urls.small);

    const masonryLinkElement = screen.getByTestId('masonry-card-link');

    expect(masonryLinkElement.className).toBe('link');
    expect(masonryLinkElement.getAttribute('href')).toBe(`/photos/${image.id}`);
    
    expect(container).toMatchSnapshot();
});

test('should render liked icon if image like is TRUE', () => {
    const { debug, container } = render(<MasonryImage image={imageLiked} />);

    const masonryLinkElement = screen.getByTestId('masonry-card-link');
    const masonryCardLikeContainer = screen.getByTestId('masonry-card-like-container');

    expect(masonryLinkElement.children.length).toBe(1);
    expect(masonryCardLikeContainer.className).toBe('like');

    expect(container).toMatchSnapshot();
});

test('should  NOT render liked icon if image like is FALSE', () => {
    const { debug, container } = render(<MasonryImage image={imageNotLiked} />);

    const masonryLinkElement = screen.getByTestId('masonry-card-link');

    expect(masonryLinkElement.children.length).toBe(0);
    expect(container).toMatchSnapshot();
});

