import React, { SyntheticEvent, useEffect, ReactNode, useState } from "react";
import ReactDOM from "react-dom";
import { useNavigate } from "react-router-dom";
import { debounce_leading } from "../../../utils/utils";
import { CloseBtn } from "../icons/close-btn/close-btn.component";

import styles from './modal.module.scss';

interface ModalPropTypes {
    children: ReactNode
}

export function Modal({ children }: ModalPropTypes) {
    const navigate = useNavigate();
    const [modalOutStyle, setModalOutStyle] = useState('');

    // little hack to handle a smooth close animation without any external UI lib
    function handleClose() {
        setModalOutStyle(styles['modal__content--out']);
        setTimeout(() => {
            /**
             * checking if user opened the modal page directly or not
             * if the history length is greater than means user navigated to this page by clicking a link
             * Then we can take advantage of navigate to send user back to previous page and keep the history clean
             * */
            if (window.history.length > 2) {
                navigate(-1);
            } else {
                navigate('/');
            }
        }, 290);
    }

    function handleModalContentClick(e: SyntheticEvent) {
        e.stopPropagation()
    }

    function hanldeESCPress(e: KeyboardEvent) {
        if (e.code && e.code === 'Escape') {
            handleClose();
        }
    }

    useEffect(() => {
        window.addEventListener('keydown', hanldeESCPress);
        document.body.classList.add('no-overflow');
        return () => {
            window.removeEventListener('keydown', hanldeESCPress);
            document.body.classList.remove('no-overflow');
        }
    });

    // little behaviour hack
    const onClose = debounce_leading(handleClose, 500);

    return ReactDOM.createPortal(
        <div
            className={styles.modal}
            onClick={onClose}
            data-testid="modal"
        >
            <div
                className={[styles['modal__content'], modalOutStyle].join(' ')}
                onClick={handleModalContentClick}
                data-testid="modal-content"
            >
                <span
                    onClick={onClose}
                    className={styles['modal--close']}
                    data-testid="modal-close-btn"
                >
                    <CloseBtn />
                </span>
                {children}
            </div>
        </div>
        ,
        document.getElementById('modal') as HTMLElement
    );
}