import { fireEvent, waitFor, waitForElementToBeRemoved } from "@testing-library/react";
import { debug } from "console";
import React from "react";
import { useNavigate } from "react-router-dom";
import { render, screen } from '../../../test-utils';
import { Modal } from "./modal.component";

const DummyComponent = () => {
    return (
        <div className="sample-dummy">this component used for testing propose only</div>
    );
};

jest.mock('react-dom', () => ({
    ...jest.requireActual('react-dom'),
    createPortal: (element: any) => element
}));

jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'),
    useNavigate: jest.fn()
}));

beforeEach(() => {

});

afterEach(() => {
    jest.clearAllMocks();
});

test('should render empty modal component', async () => {
    const { container, debug } = render(<Modal> </Modal>);

    await waitFor(() => screen.getByTestId('modal'));

    const modalComponent = screen.getByTestId('modal');
    expect(modalComponent).toBeInTheDocument();

    const modalContentComponet = screen.getByTestId('modal-content');
    expect(modalContentComponet.children.length).toBe(1);
});

test('should render close btn should close the modal', async () => {
    const { container, debug } = render(<Modal> </Modal>);

    await waitFor(() => screen.getByTestId('modal'));

    const modalCloseBtnComponent = screen.getByTestId('modal-close-btn');

    fireEvent(modalCloseBtnComponent,
        new MouseEvent('click', {
            bubbles: false,
            cancelable: true,
        }));

    expect(modalCloseBtnComponent).toBeInTheDocument();

    expect(useNavigate).toBeCalledTimes(1);
    expect(container).toMatchSnapshot();
});

test('should render the child properly inside Modal', async () => {
    const { container, debug } = render(<Modal> <DummyComponent/> </Modal>);

    await waitFor(() => screen.getByTestId('modal-content'));

    const modalCloseBtnComponent = screen.getByTestId('modal-close-btn');

    fireEvent(modalCloseBtnComponent,
        new MouseEvent('click', {
            bubbles: false,
            cancelable: true,
        }));

    const dummyChild = screen.getByText(/this component used for testing propose only/i);
    expect(dummyChild).toBeInTheDocument();

    expect(container).toMatchSnapshot();
});