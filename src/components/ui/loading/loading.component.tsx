import React from "react";

import styles from './loading.module.scss';

export function Loading() {
    return (
        <div>
            <h4 className={styles.loading}>Loading...</h4>
        </div>
    );
}