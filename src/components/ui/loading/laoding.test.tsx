import React from "react";
import { render, screen } from '../../../test-utils';
import { Loading } from "./loading.component";

test('should render a simple laoding component', () => {

    const { debug, container } = render(<Loading />);

    const LoadingElement = screen.getByText(/Loading.../i);
    expect(LoadingElement).toBeInTheDocument();
    expect(LoadingElement?.className).toBe('loading');
    expect(container).toMatchSnapshot();
});
