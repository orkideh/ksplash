import React from "react";
import { images } from "../../../assets/mocks/images";
import { render, screen } from '../../../test-utils';
import { MyImage } from "../../../types/types";
import { ImageModalExif } from "./image-modal-exif.component";

let image: MyImage;

beforeEach(() => {
    image = images[0];
});

test('should render image modal exif component', () => {
    const { container, debug } = render(<ImageModalExif exif={image.exif} />);

    const modalSeperatorElement = screen.getByTestId('exif-modal-seperator');
    const exifListComponent = screen.getByTestId('image-modal-exif');

    expect(container.children.length).toBe(2);
    expect(exifListComponent.children.length).toBe(6);
    
    expect(container).toMatchSnapshot();
    expect(modalSeperatorElement).toMatchSnapshot();
});

test('should render image modal exif component with correct props', () => {
    const { container, debug } = render(<ImageModalExif exif={image.exif} />);

    const exifMakeElement = screen.getByText(`${image.exif.make}`);
    expect(exifMakeElement).toBeInTheDocument();

    const exifModelElement = screen.getByText(`${image.exif.model}`);
    expect(exifModelElement).toBeInTheDocument();

    const exifDocalLengthElement = screen.getByText(`${image.exif.focal_length}`);
    expect(exifDocalLengthElement).toBeInTheDocument();

    const exifAperturelement = screen.getByText(`${image.exif.aperture}`);
    expect(exifAperturelement).toBeInTheDocument();

    const exifExposureTimelement = screen.getByText(`${image.exif.exposure_time}`);
    expect(exifExposureTimelement).toBeInTheDocument();

    const exifISOlement = screen.getByText(`${image.exif.iso}`);
    expect(exifISOlement).toBeInTheDocument();
    expect(container).toMatchSnapshot();
});

