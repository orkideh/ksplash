import React from "react";
import { MyImageWithLike } from "../../../types/types";

import styles from './image-modal-exif.module.scss';

interface ImageExifProps {
    exif: MyImageWithLike['exif']
};

export function ImageModalExif({ exif }: ImageExifProps) {
    return (
        <>
            <div
                className={styles['seperator']}
                data-testid="exif-modal-seperator"
            ></div>
            <ul
                className={styles.list}
                data-testid="image-modal-exif"
            >
                <li>
                    <h6>Camera make</h6>
                    <p
                        data-testid="exif-make"
                    >
                        {exif.make}
                    </p>
                </li>
                <li>
                    <h6>Camera model</h6>
                    <p
                        data-testid="exif-model"
                    >
                        {exif.model}
                    </p>
                </li>
                <li>
                    <h6>Focal length</h6>
                    <p
                        data-testid="exif-focal-length"
                    >
                        {exif.focal_length}
                    </p>
                </li>
                <li>
                    <h6>Aperture</h6>
                    <p
                        data-testid="exif-aperture"
                    >
                        {exif.aperture}
                    </p>
                </li>
                <li>
                    <h6>Exposure time</h6>
                    <p
                        data-testid="exif-exposure-time"
                    >
                        {exif.exposure_time}
                    </p>
                </li>
                <li>
                    <h6>ISO</h6>
                    <p
                        data-testid="exif-iso"
                    >
                        {exif.iso}
                    </p>
                </li>
            </ul>
        </>
    );
}