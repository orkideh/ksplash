import React from "react";
import { MyImage } from "../../../types/types";
import { generateSrcSet } from "../../../utils/utils";

import styles from './responsive-modal-image.module.scss';

interface ResponsiveModalImageProps {
    image: MyImage
}

export function ResponsiveModalImage({ image }: ResponsiveModalImageProps) {
    const srcSet = generateSrcSet(image.urls);
    const imageAlt = image.description || image.urls.full;
    return (
        <img
            data-testid="responsive-image"
            src={image.urls.full}
            srcSet={srcSet}
            className={styles.responsive}
            alt={imageAlt}
        />
    );
}