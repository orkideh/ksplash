import React from "react";
import { images } from "../../../assets/mocks/images";
import { render, screen } from '../../../test-utils';
import { MyImageWithLike } from "../../../types/types";
import { ResponsiveModalImage } from "./responsive-modal-image.component";

let image: MyImageWithLike;
beforeEach(() => {
    image = Object.assign({}, images[0], { like: true })
});

test('should render responsive image component', () => {
    const { container, debug } = render(<ResponsiveModalImage image={image} />);

    const imageElement = screen.getByTestId('responsive-image');

    expect(imageElement.className).toBe('responsive');
    expect(imageElement.getAttribute('src')).toBe(image.urls.full);
    expect(container).toMatchSnapshot();
});

test('should render alt matching image description if image description has been set', () => {
    image.description = image.description || 'a test description for image';
    const { container, debug } = render(<ResponsiveModalImage image={image} />);

    const imageElement = screen.getByAltText(image.description);
    expect(imageElement).toBeInTheDocument();
    expect(container).toMatchSnapshot();
});

test('should render alt matching image url if image description has NOT been set', () => {
    image.description = '';
    const { container, debug } = render(<ResponsiveModalImage image={image} />);

    const imageElement = screen.getByAltText(image.urls.full);
    expect(imageElement).toBeInTheDocument();
    expect(container).toMatchSnapshot();
});
