
import React from "react";
import { Link } from "react-router-dom";
import styles from './four-o-four.module.scss';

export function FourOFour(){
    
    return (
        <div>
            Unfortunately your requested page has not found
            <Link to="/">Go to the home page</Link>
        </div>
    );
}