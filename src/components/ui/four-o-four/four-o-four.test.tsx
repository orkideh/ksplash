import React from "react";
import { render, screen } from '../../../test-utils';
import { FourOFour } from "./four-o-four.component";

test('should render custom Four O Four component', () => {
    const { container } = render(<FourOFour />);

    const fourOFourElement = screen.getByText(/Unfortunately your requested page has not found/i);
    expect(fourOFourElement).toBeInTheDocument();
    expect(container).toMatchSnapshot();

});

test('should render corrent link to home page', () => {
    const { debug, container } = render(<FourOFour />);

    const fourOFourElement = screen.getByText(/Unfortunately your requested page has not found/i);
    const linkElement = screen.getByText(/Go to the home page/i);

    expect(linkElement.getAttribute('href')).toBe('/');
    expect(container).toMatchSnapshot();
});