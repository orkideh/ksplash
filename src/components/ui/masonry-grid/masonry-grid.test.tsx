import React from "react";
import { images } from "../../../assets/mocks/images";
import { render, screen } from '../../../test-utils';
import { MyImageWithLike } from "../../../types/types";
import { randomNumberRange } from "../../../utils/utils";
import { MasonryGrid } from "./masonry-grid.component";


let imageWithLikes: MyImageWithLike[];
beforeEach(() => {
    imageWithLikes = images.map(image => ({ ...image, like: false }));
});

test('should render all the images passed into', () => {
    const { debug, container } = render(<MasonryGrid images={imageWithLikes} />);

    const masonaryGrid = screen.getByTestId('masonry-grid');

    expect(masonaryGrid.children.length).toEqual(images.length);
    expect(container).toMatchSnapshot();
});


test('should render a specified element with like button if like props is TRUE', () => {
    const index = 13;
    imageWithLikes[index].like = true;
    const { debug, container } = render(<MasonryGrid images={imageWithLikes} />);

    expect(container.children[0].children[index].children[0].childNodes.length).toEqual(1);
    expect(container.children[0].children[index].children[0]?.children[0].className).toEqual('like');
    expect(container).toMatchSnapshot();
});

test('should NOT render a specified element with like button if like props is FALSE', () => {
    const index = 13;
    imageWithLikes[index].like = false;
    const { debug, container } = render(<MasonryGrid images={imageWithLikes} />);

    expect(container.children[0].children[index].children[0].children.length).toEqual(0);
    expect(container).toMatchSnapshot();
});
