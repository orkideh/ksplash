import React from "react";
import { MyImageWithLike } from "../../../types/types";
import { MemorizedMasonryImage } from "../masonry-image/masonry-image.component";

import styles from './masonry-grid.module.scss';

interface MasonryGridProps {
    images: MyImageWithLike[]
}

export function MasonryGrid({images}: MasonryGridProps) {
    return (
        <div
         className={styles['flex-mosaic']}
         data-testid="masonry-grid"
         >
            {images.length ? images.map(image => <MemorizedMasonryImage key={image.id} image={image} />) : 'There is no item to render'}
            {/* {images.map(image => <MemorizedMasonryImage key={image.id} image={image} />)} */}
        </div>
    );
}
