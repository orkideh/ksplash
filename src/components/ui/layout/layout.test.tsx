import React from "react";
import { render, screen } from '../../../test-utils';
import { Layout } from "./layout.component";


test('should render a simple layout component with default props', () => {

    const { debug, container } = render(<Layout> </Layout>);

    const LayoutElement = screen.getByTestId('container');
    
    expect(LayoutElement).toBeInTheDocument();
    expect(LayoutElement.className).toBe('container');
    expect(container).toMatchSnapshot();
});


test('should render a simple layout component with fluid props TRUE', () => {

    const { debug, container } = render(<Layout fluid={true}> </Layout>);

    const LayoutElement = screen.getByTestId('container');

    expect(LayoutElement).toBeInTheDocument();
    expect(LayoutElement.className).toBe('container-fluid');
    expect(container).toMatchSnapshot();
});

test('should render a simple layout component with childrens passed into it', () => {

    const DummyComponent = () => {
        return (
            <div className="sample-dummy">this component used for testing propose only</div>
        );
    };

    const { debug, container } = render(<Layout> <DummyComponent /> </Layout>);

    const childElement = screen.getByText(/this component used for testing propose only/i);
    expect(childElement).toBeInTheDocument();
    expect(container).toMatchSnapshot();
});
