import React, { ReactNode } from "react";

import styles from './layout.module.scss'

interface LayoutProps {
    children: ReactNode
    fluid?: boolean
}

export function Layout({ children, fluid = false }: LayoutProps) {
    return (
        <div
            data-testid="container"
            className={fluid ? styles['container-fluid'] : styles.container}
        >
            {children}
        </div>
    );
}