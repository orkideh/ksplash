import React from "react";
import { render, screen } from '../../../../test-utils';
import { LogoMenu } from "./logo-menu.component";

test('should render a simple close Logo icon component', () => {
    const { container, debug } = render(<LogoMenu />);

    const closeBtnElement = screen.getByTestId('logo-menu');
    expect(closeBtnElement.tagName.toLowerCase()).toBe('svg');
    expect(container).toMatchSnapshot();
});