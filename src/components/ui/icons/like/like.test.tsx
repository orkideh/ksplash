import React from "react";
import { render, screen } from '../../../../test-utils';
import { Like } from "./like.component";

test('should render a simple Like icon component', () => {
    const { container, debug } = render(<Like />);

    const likelement = screen.getByTestId('like');
    expect(likelement.tagName.toLowerCase()).toBe('svg');
    expect(container).toMatchSnapshot();
});