import React from "react";

export function Like() {
    return (
        <svg data-testid="like" width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect width="32" height="32" rx="10" />
            <path d="M15.8406 21.9027C14.394 21.012 13.0473 19.964 11.826 18.7767C10.9673 17.922 10.314 16.88 9.91532 15.73C9.19798 13.5 10.0353 10.9473 12.3807 10.192C13.6133 9.79467 14.9587 10.022 15.9973 10.8007C17.036 10.0227 18.3813 9.79601 19.614 10.192C21.9586 10.9473 22.8026 13.5 22.0853 15.73C21.6866 16.88 21.0333 17.922 20.1746 18.7767C18.9533 19.9633 17.6073 21.012 16.16 21.9027L16.0033 22L15.8406 21.9027V21.9027Z" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
            <path d="M18.4933 12.702C19.2033 12.9287 19.7073 13.5667 19.7706 14.3167" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
        </svg>

    );
}