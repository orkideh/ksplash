import React from "react";
import { render, screen } from '../../../../test-utils';
import { HomeIcon } from "./images-icon.component";

test('should render a simple Home icon component', () => {
    const { container, debug } = render(<HomeIcon />);

    const homeElement = screen.getByTestId('home');
    expect(homeElement.tagName.toLowerCase()).toBe('svg');
    expect(container).toMatchSnapshot();
});