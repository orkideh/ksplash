import React from "react";
import { render, screen } from '../../../../test-utils';
import { CloseBtn } from "./close-btn.component";

test('should render a simple close button icon component', () => {
    const { container, debug } = render(<CloseBtn />);

    const closeBtnElement = screen.getByTestId('close-btn');
    expect(closeBtnElement.tagName.toLowerCase()).toBe('svg');
    expect(container).toMatchSnapshot();
});