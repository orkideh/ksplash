import React from 'react';
import './app.module.scss';
import { Home } from './components/home/home.component';
import { Route, Routes } from 'react-router-dom';
import { FourOFour } from './components/ui/four-o-four/four-o-four.component';
import { ImageModal } from './components/image-modal/image-modal.component';

function App() {
  return (
    <>
      <Routes>
        <Route path='/' element={<Home />} >
          <Route path='photos/:id' element={<ImageModal />} />
          <Route path='*' element={<FourOFour />} />
        </Route>
      </Routes>
    </>
  );
}

export default App;
