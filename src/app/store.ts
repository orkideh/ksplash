import { configureStore } from '@reduxjs/toolkit'
import favoritesSlice from '../feature/favorites-slice';
import { imageApiSlice } from '../feature/images/images-api-slice'

export const store = configureStore({
    reducer: {
        favorites: favoritesSlice,
        [imageApiSlice.reducerPath]: imageApiSlice.reducer
    },
    middleware: (getDefaultMiddleware) => {
        return getDefaultMiddleware().concat(imageApiSlice.middleware);
    }
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>
