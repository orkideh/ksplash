import { useState } from 'react';
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
import { addToFavorite, removeFromFavorite, selectImageById } from '../feature/favorites-slice';
import { useFetchImageQuery, useFetchImagesQuery } from '../feature/images/images-api-slice';
import { MyImageWithLike } from '../types/types';
import { mapImagesWithLikes } from '../utils/utils';
import { RootState, AppDispatch } from './store';

export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector  

export const useImageView = () => {
    const [viewState, setViewState] = useState('home');
    const isHome = viewState === 'home';
    const { data = [], isFetching, isError, isLoading } = useFetchImagesQuery();
    const favorites = useAppSelector(state => state.favorites);

    const imagesWithLikes = isHome ? mapImagesWithLikes(data, favorites): favorites;

    return {
        images: imagesWithLikes,
        isHome,
        setHomeView: () => setViewState('home'),
        setFavoriteView: () => setViewState('favorite'),
        homeStatus: {
            isFetching,
            isError,
            isLoading
        }
    }
};

export function useImage(id: MyImageWithLike['id']) {
    const dispatch = useAppDispatch();

    const { data, isFetching, isLoading, isError } = useFetchImageQuery(id);

    const liked = useAppSelector(selectImageById(id)) ? true : false;
    const image = Object.assign({}, data, { like: liked });

    function handleToggle() {
        if(image.like) {
            dispatch(removeFromFavorite(image))
        } else {
            dispatch(addToFavorite(image))
        }
    }

    return {
        image,
        isError,
        isFetching,
        isLoading,
        handleToggle
    };

}