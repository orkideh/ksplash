// test-utils.jsx
import React from 'react'
import { render as rtlRender } from '@testing-library/react'
import { Provider } from 'react-redux'
import { BrowserRouter, MemoryRouter } from 'react-router-dom';
// Import your own reducer
import { configureStore } from '@reduxjs/toolkit';
import favoritesSlice from './feature/favorites-slice';
import { imageApiSlice } from './feature/images/images-api-slice';
import { images } from './assets/mocks/images';
import { store } from './app/store';

const customeWrapper = (activeRoute) => ({ children }) => {
    return (
        <MemoryRouter initialEntries={[activeRoute]}>
            <Provider store={store}>
                {children}
            </Provider>
        </MemoryRouter>
    );
};


function render(ui, activeRoute = '/', options = {}) {
    return rtlRender(ui, { wrapper: customeWrapper(activeRoute), ...options })
}

// re-export everything
export * from '@testing-library/react'
// override render method
export { render }