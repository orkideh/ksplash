
# Getting Started with Ksplash
- Thank you for spending time and reviewing my assigment.
- `yarn`
- Check `src/config/config.json` to check if dev server is enabled.
- if the dev server is enabled then start the dev server first by `yarn mock:run`.
- `yarn start` in order to run front-end app.

## Requirements
- [x] Use React JS framework (create-react-app is fine).
- [x] Rebuild the layout as in the design(`+ a custom favicon`).
- [x] Retrieve images from Unsplash API and list them in the grid.
- [x] Use react-router to display a specific image by id parameter.
- [x] Styling of components is written in SCSS/SASS + BEM.
- [x] The code should be written using Typescript.
- [x] The app must be responsive, mobile and desktop designs are provided.
- [x] Do not use any UI libraries (e.g. Bootstrap, Bulma, Material UI, …).

## Extra credit
- [x] Use redux to save liked images (locally, not on unsplash api).
- [x] Use localstorage to save the liked images then rehydrate redux store on browser refresh.
- [x] Unit tests -> only a few test remains due to time constraints.
- [x] Transitions & animations -> done but I think can be implimented better using other libs.
- [x] Custom webpack config -> integrated [craco](https://github.com/gsoft-inc/craco) it is possible to customize it but did not fell the urge to add any customizations.
- [x] Works consistently on all modern browsers (Chrome, Firefox, Edge(Webkit), Safari(optional) -> check on Chrome, Firefox, Safari and Chrome Android.

## Extra Extra points :D
- [x] Developed a simple isomorphoc file based config. that can be best parctice to share a config across different apps which can be protected by files system permission. Also suitable for CI/CD. include
````
{
    "dev": {
        "enabled": true,
        "port": 8080,
        "host": "localhost"
    },
    "auth": {
        "token": "zdtscgj_S1QN4BBKFLbwYI0kUg7DlpzJscLT4pfjKzs"
    }
}
````

- [x] Due to Unsplash API limited rate limit developed a mock server with some sample data from API to improve productivit. Mock functionality can be disabled/enabled in config file by `dev.enabled = true` or `dev.enabled = true`, and run with `yarn mock:run`. There is point regarding `port` if `port` has changed the `package.json -> proxy` entry needs to be updated manually too.
- [x] Implimented part of the state with RTK query(basic usage + immer).
- [x] Editor config added to increase productivity and encourage the standard for future development as a best practice.
- [x] Handled API errors, Loading state and empty list rendering(basic).
- [x] File/folder name convention
````
component-name/
    component-name.component.tsx
    component-name.module.scss
    component-name.test.tsx
````
- [x] User is able to close the modal by pressing `escape` too.

## Points to consider
- [] regarding modal image first assumtion was image will take always center left side of modal(left to right), but I found it that may cause some issues with different image sizes that is why I have developed it more more flexible not bound it to hard coded dimentions.

## Nice to do
- [] To sanitize data passed to components as some are image sources and and urls
- [] To check optimal rendering(if components are not rendring if it is not necessary). Could not prioritize it due to time constraints.

