const express = require('express');
const app = express();
const cors = require('cors');
const config = require('../src/config/config.json');
/**
 * Due to very limietd rate on the API I made this faker to server images to icrease productivity
 */
 const staticImages = require('./images.json');

 
const PORT = 8080;
app.use(cors());


function getDelyed(keke){
    const timeout = Math.floor(Math.random() * 10000);
    return new Promise(resolve => {
        setTimeout(() => {
        resolve(keke)
    }, timeout);
    });
}

app.get('/photos/random', (req, res) => {
    // getDelyed(staticImages).then(item => res.json(item))
    res.json(staticImages);
})
  
app.get('/photos/:id', (req, res) => {
    const id = req.params.id;
    const image = staticImages.find(image => image.id === id);
    if(image) {
        // getDelyed(image).then(item => res.json(item))
        res.json(image);
    } else {
        res.sendStatus(404).send();
    }
})

app.listen(config.dev.port, () => {
    console.log(`mock up server listening on port ${config.dev.port}`)
})

